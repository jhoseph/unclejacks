
var System = {
    environment: 'development'
}

System.Module = {
    
    load: function(name){
        this.load_module_css(name);
        this.load_module_js(name);
                  
    },
        
    load_module_js: function(name){
        var url =  'modules/'+name+'/'+name+'.js';
        
        var nodes = $("head");
        
        if(!nodes[0]){
            return;
        }
        var headID = nodes[0];
        
        var jsElement = document.createElement("script");
        jsElement.type = "text/javascript";
        jsElement.src = url;
        jsElement.id = 'js_'+name;
        
        // jsElement.appendChild(document.createTextNode(data));
        
        headID.appendChild(jsElement);
    },
        
    load_module_css: function(name){
        var url =  'modules/'+name+'/'+name+'.css';
        
        var nodes = $("head");
        
        if(!nodes[0]){
            return;
        }
        var headID = nodes[0];
        
        var jsElement = document.createElement("link");
        jsElement.type = "text/css";
        jsElement.href = url;
         jsElement.rel = 'stylesheet';
        jsElement.id = 'css_'+name;
        
        // jsElement.appendChild(document.createTextNode(data));
        
        headID.appendChild(jsElement);
    }
    
}


System.log = function(message){
    //console.log(message);
}

/**
 * View is the class for renderign content using mustache
 */
System.View = function(view){
    System.log('setting up view..');
    this.view = view;
    this.render = function(target, data){
        System.log('redering '+this.view+' on '+target);
        var view_data = data;
        var url = 'modules/'+this.view+'.html';
    
        $.ajax(
        {
            url: url,
            async: false,
            success:function(view_text){
                System.log('view found!');
                if(!view_data){
                    System.log('No data so no rendering');
                    var rendered = view_text;
                }
                else{
                    System.log('Mustache-Rendering data');
                    var rendered = Mustache.render(view_text, view_data);
                }
                System.log('rendered vew: '+rendered);
                $(target).html(rendered);
                System.helpers.setViewEvents();
            },
            
            
            
            error: function(data){
                System.log('error:View not found');
            }
        });
    }
    
}

System.helpers = {};
System.helpers.render = function(view_name, target, data){
    System.log('rendering '+view_name+' on '+target);
    var view =  new System.View(view_name);
    System.log('view set, rendering..');
    view.render(target, data);
}

var Views = {
    render: System.helpers.render
}
System.helpers.setViewEvents=function(){
    $('.click_action').each(function() {
        var click_action = $(this).attr('click_action');
       
        
        this.ontouchstart = function(event) {
            $(this).removeClass('touched');
            //$(this).addClass('touched');
            
        };
        this.onclick = function(event){
            System.log('clicked');
            
        };
        this.ontouchend = function(event) {
            
            
            eval(click_action);
            $(this).removeClass('touched');
        };
        $(this).removeClass('click_action');
      } );  
}



var Service = {
        show_info_box: function(text){
            if(!text){ text = 'Loading...'; }
            $('.info_box').html('<p class="infobox_text">'+text+'</p>');
            $('.info_box').show();  
        },
        hide_info_box: function(){
            $('.info_box').html('');
	    $('.info_box').hide();
        },
	token: '',
        /*
         https://graph.facebook.com/oauth/access_token?client_id=262050363861602&client_secret=afe0a998584593789855a803f9e4919d&grant_type=client_credentials
        */
        fb_token: '262050363861602%7C91JnnF6KAsdm9ZlXGT1kuFEBH2U', // set by Login
	post: function(service_name, data, callback){ return Service._call('post',System.Main.get_server(),service_name, data, callback, false, false); },
	get: function(service_name, data, callback){ return Service._call('get',System.Main.get_server(),service_name, data, callback, false, false); },
        post_with_no_loading: function(service_name, data, callback){return Service._call('post',System.Main.get_server(),service_name, data, callback, false, true);},
	
        get_with_no_loading: function(service_name, data, callback){return Service._call('get',System.Main.get_server(),service_name, data, callback, false, true);},
	_call:function(call_type,url,service_name, data, callback, fail_callback, do_not_show_loading_box){
                
		if(!do_not_show_loading_box)
                {
                    $('.info_box').html('<p class="infobox_text">Loading...</p>');
                    $('.info_box').show();
                }
                
                //document.getElementById("dev_tools").focus();
                 $('.info_box').each(function() {
                    
                    this.ontouchstart = function(event) {
                        $('.info_box').hide();
                    };
                  } );
                System.log(url);
                System.log(Service.token);
		$.ajax({
			url: url+service_name,
			type: call_type,
			data: {'data':$.toJSON(data),'token':Service.token},
                       // dataType: 'json',
			success: function(response) {
			    
                            System.log(response.substring(1,400));
                            
			   callback($.evalJSON(response));
			   $('.info_box').html('');
			   $('.info_box').hide();
			},
			error: function(error){
			    
                                if(fail_callback)
                                {
                                    fail_callback(error);
                                    return ;
                                }
				//$('.info_box').html('Connection error...');
                                System.log(error);
				return ;
			}
		});
	},
        fb_get: function(service_name, data, callback){
            if(!data){data = {} }            
            data['access_token'] = Service.fb_token;
            var out = new Array();

            for (key in data) {
                out.push(key + '=' + data[key]);
            }
            
            var extra = out.join('&');
            System.log('https://graph.facebook.com/'+service_name+'?'+extra);
            return Service._call('get','https://graph.facebook.com/',service_name+'?'+extra, {}, callback,
                function(response){
                    
                    response = $.evalJSON(response['responseText']);
                    if(response['error'])
                    {
                        $('.info_box').html('Connection error...');
			System.log('fb_error');
                        System.log(response['error']);
                        return;
                    }
                    $('.info_box').html('');
                    $('.info_box').hide();
                    callback(response['data']);
                    return;
                }
            );
        }
};

var OO = {
    create: function(object_array){
        var ret = function(arga,argb){
            for( func in object_array ){
                this[func] = object_array[func];
            }
            if(this['init']){
                this['init'](arga,argb);
            }
        }
        return ret;
    }
}

System.Main = {
	service_path: '/app_service/uni/',
	get_server: function(){
		if(NoGap && NoGap.servers){
			return NoGap.servers[NoGap.current_environment]+System.Main.service_path;
		}
		return this.service_path;
	},
	
	
};

var Remote = OO.create({
    cache : {},
    init: function(params){
        this.type   = params.type? params.type: 'post';
        this.base_url = params.base_url? params.base_url: 'http://www.inthaweb.com/mooseheads/';
        this.extra_data = params.extra_data? params.extra_data: {};
    },
    call: function(params){
        params = this.update_data(params);
        
        if(params.cache){
            var md5  = MD5($.toJSON([params.service, params.data]));
            
            if(this.cache[md5]){
                return this.cache[md5];
            }
        }
        System.log('creating url ...')
        var url = this.create_url(params);
        System.log(url);
        $.ajax({
			url: url,
			type: this.type,
                        
			data: {'data':$.toJSON(params.data),'token':Service.token},
                       // dataType: 'json',
			success: function(response) {
                            //System.log(response);
                            System.log('got it, now calling params');
                            System.log(params);
                            params.callback(response);
                            this.cache[md5] = response;
			   $('.info_box').html('');
			   $('.info_box').hide();
			},
			error: function(error){
                                if(params.fail_callback)
                                {
                                    params.fail_callback(error);
                                    return ;
                                }
				//$('.info_box').html('Connection error...');
                                System.log('error');
				return ;
			}
		});
        
        
        return true;
    },
    update_data: function(params){
        if(!params.data){params.data = {} }   
        for(x in this.extra_data)
        {
            params.data[x] =  this.extra_data[x];
        }
        
        return params;
    },
    create_url: function(params)
    {
        if(this.type == 'get'){
                        
            
            var out = new Array();
            System.log('its a get method, reating the url concatenating the data');
            for (key in params.data) {
                out.push(key + '=' + params.data[key]);
            }
            
            var extra = out.join('&');
            return this.base_url+params.service+'?'+extra;
        }
        
        else{
            return this.base_url+params.service;
        }
    }
    
    
});




var NoGap = {
	
    /**
     change this when deploying options are the server keys, example: 'production', 'development'
     **/
    current_environment: 'production',
        /**
         this can be either 'local', 'remote', ONLY used on development
         */
    get_app_data_from: 'local', 
        /**
         This is where the application script will be found in the server
         **/
    app_path: '/app_pack/mooseheads',
        
        /**
         List of possible servers for the NoGap environment
         **/ 
    servers: { 	
    production:  'http://whatsonuni.appspot.com',
    development: 'http://127.0.0.1:8088'
    },
        
        
    app_data_filename: 'app_data_local',
        
    get_app_url: function(){
        return NoGap.servers[NoGap.current_environment]+NoGap.app_path;
    },
        
   
	
}


var MD5 = function (string) {

   function RotateLeft(lValue, iShiftBits) {
           return (lValue<<iShiftBits) | (lValue>>>(32-iShiftBits));
   }

   function AddUnsigned(lX,lY) {
           var lX4,lY4,lX8,lY8,lResult;
           lX8 = (lX & 0x80000000);
           lY8 = (lY & 0x80000000);
           lX4 = (lX & 0x40000000);
           lY4 = (lY & 0x40000000);
           lResult = (lX & 0x3FFFFFFF)+(lY & 0x3FFFFFFF);
           if (lX4 & lY4) {
                   return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
           }
           if (lX4 | lY4) {
                   if (lResult & 0x40000000) {
                           return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
                   } else {
                           return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
                   }
           } else {
                   return (lResult ^ lX8 ^ lY8);
           }
   }

   function F(x,y,z) { return (x & y) | ((~x) & z); }
   function G(x,y,z) { return (x & z) | (y & (~z)); }
   function H(x,y,z) { return (x ^ y ^ z); }
   function I(x,y,z) { return (y ^ (x | (~z))); }

   function FF(a,b,c,d,x,s,ac) {
           a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
           return AddUnsigned(RotateLeft(a, s), b);
   };

   function GG(a,b,c,d,x,s,ac) {
           a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
           return AddUnsigned(RotateLeft(a, s), b);
   };

   function HH(a,b,c,d,x,s,ac) {
           a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
           return AddUnsigned(RotateLeft(a, s), b);
   };

   function II(a,b,c,d,x,s,ac) {
           a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
           return AddUnsigned(RotateLeft(a, s), b);
   };

   function ConvertToWordArray(string) {
           var lWordCount;
           var lMessageLength = string.length;
           var lNumberOfWords_temp1=lMessageLength + 8;
           var lNumberOfWords_temp2=(lNumberOfWords_temp1-(lNumberOfWords_temp1 % 64))/64;
           var lNumberOfWords = (lNumberOfWords_temp2+1)*16;
           var lWordArray=Array(lNumberOfWords-1);
           var lBytePosition = 0;
           var lByteCount = 0;
           while ( lByteCount < lMessageLength ) {
                   lWordCount = (lByteCount-(lByteCount % 4))/4;
                   lBytePosition = (lByteCount % 4)*8;
                   lWordArray[lWordCount] = (lWordArray[lWordCount] | (string.charCodeAt(lByteCount)<<lBytePosition));
                   lByteCount++;
           }
           lWordCount = (lByteCount-(lByteCount % 4))/4;
           lBytePosition = (lByteCount % 4)*8;
           lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80<<lBytePosition);
           lWordArray[lNumberOfWords-2] = lMessageLength<<3;
           lWordArray[lNumberOfWords-1] = lMessageLength>>>29;
           return lWordArray;
   };

   function WordToHex(lValue) {
           var WordToHexValue="",WordToHexValue_temp="",lByte,lCount;
           for (lCount = 0;lCount<=3;lCount++) {
                   lByte = (lValue>>>(lCount*8)) & 255;
                   WordToHexValue_temp = "0" + lByte.toString(16);
                   WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length-2,2);
           }
           return WordToHexValue;
   };

   function Utf8Encode(string) {
           string = string.replace(/\r\n/g,"\n");
           var utftext = "";

           for (var n = 0; n < string.length; n++) {

                   var c = string.charCodeAt(n);

                   if (c < 128) {
                           utftext += String.fromCharCode(c);
                   }
                   else if((c > 127) && (c < 2048)) {
                           utftext += String.fromCharCode((c >> 6) | 192);
                           utftext += String.fromCharCode((c & 63) | 128);
                   }
                   else {
                           utftext += String.fromCharCode((c >> 12) | 224);
                           utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                           utftext += String.fromCharCode((c & 63) | 128);
                   }

           }

           return utftext;
   };

   var x=Array();
   var k,AA,BB,CC,DD,a,b,c,d;
   var S11=7, S12=12, S13=17, S14=22;
   var S21=5, S22=9 , S23=14, S24=20;
   var S31=4, S32=11, S33=16, S34=23;
   var S41=6, S42=10, S43=15, S44=21;

   string = Utf8Encode(string);

   x = ConvertToWordArray(string);

   a = 0x67452301; b = 0xEFCDAB89; c = 0x98BADCFE; d = 0x10325476;

   for (k=0;k<x.length;k+=16) {
           AA=a; BB=b; CC=c; DD=d;
           a=FF(a,b,c,d,x[k+0], S11,0xD76AA478);
           d=FF(d,a,b,c,x[k+1], S12,0xE8C7B756);
           c=FF(c,d,a,b,x[k+2], S13,0x242070DB);
           b=FF(b,c,d,a,x[k+3], S14,0xC1BDCEEE);
           a=FF(a,b,c,d,x[k+4], S11,0xF57C0FAF);
           d=FF(d,a,b,c,x[k+5], S12,0x4787C62A);
           c=FF(c,d,a,b,x[k+6], S13,0xA8304613);
           b=FF(b,c,d,a,x[k+7], S14,0xFD469501);
           a=FF(a,b,c,d,x[k+8], S11,0x698098D8);
           d=FF(d,a,b,c,x[k+9], S12,0x8B44F7AF);
           c=FF(c,d,a,b,x[k+10],S13,0xFFFF5BB1);
           b=FF(b,c,d,a,x[k+11],S14,0x895CD7BE);
           a=FF(a,b,c,d,x[k+12],S11,0x6B901122);
           d=FF(d,a,b,c,x[k+13],S12,0xFD987193);
           c=FF(c,d,a,b,x[k+14],S13,0xA679438E);
           b=FF(b,c,d,a,x[k+15],S14,0x49B40821);
           a=GG(a,b,c,d,x[k+1], S21,0xF61E2562);
           d=GG(d,a,b,c,x[k+6], S22,0xC040B340);
           c=GG(c,d,a,b,x[k+11],S23,0x265E5A51);
           b=GG(b,c,d,a,x[k+0], S24,0xE9B6C7AA);
           a=GG(a,b,c,d,x[k+5], S21,0xD62F105D);
           d=GG(d,a,b,c,x[k+10],S22,0x2441453);
           c=GG(c,d,a,b,x[k+15],S23,0xD8A1E681);
           b=GG(b,c,d,a,x[k+4], S24,0xE7D3FBC8);
           a=GG(a,b,c,d,x[k+9], S21,0x21E1CDE6);
           d=GG(d,a,b,c,x[k+14],S22,0xC33707D6);
           c=GG(c,d,a,b,x[k+3], S23,0xF4D50D87);
           b=GG(b,c,d,a,x[k+8], S24,0x455A14ED);
           a=GG(a,b,c,d,x[k+13],S21,0xA9E3E905);
           d=GG(d,a,b,c,x[k+2], S22,0xFCEFA3F8);
           c=GG(c,d,a,b,x[k+7], S23,0x676F02D9);
           b=GG(b,c,d,a,x[k+12],S24,0x8D2A4C8A);
           a=HH(a,b,c,d,x[k+5], S31,0xFFFA3942);
           d=HH(d,a,b,c,x[k+8], S32,0x8771F681);
           c=HH(c,d,a,b,x[k+11],S33,0x6D9D6122);
           b=HH(b,c,d,a,x[k+14],S34,0xFDE5380C);
           a=HH(a,b,c,d,x[k+1], S31,0xA4BEEA44);
           d=HH(d,a,b,c,x[k+4], S32,0x4BDECFA9);
           c=HH(c,d,a,b,x[k+7], S33,0xF6BB4B60);
           b=HH(b,c,d,a,x[k+10],S34,0xBEBFBC70);
           a=HH(a,b,c,d,x[k+13],S31,0x289B7EC6);
           d=HH(d,a,b,c,x[k+0], S32,0xEAA127FA);
           c=HH(c,d,a,b,x[k+3], S33,0xD4EF3085);
           b=HH(b,c,d,a,x[k+6], S34,0x4881D05);
           a=HH(a,b,c,d,x[k+9], S31,0xD9D4D039);
           d=HH(d,a,b,c,x[k+12],S32,0xE6DB99E5);
           c=HH(c,d,a,b,x[k+15],S33,0x1FA27CF8);
           b=HH(b,c,d,a,x[k+2], S34,0xC4AC5665);
           a=II(a,b,c,d,x[k+0], S41,0xF4292244);
           d=II(d,a,b,c,x[k+7], S42,0x432AFF97);
           c=II(c,d,a,b,x[k+14],S43,0xAB9423A7);
           b=II(b,c,d,a,x[k+5], S44,0xFC93A039);
           a=II(a,b,c,d,x[k+12],S41,0x655B59C3);
           d=II(d,a,b,c,x[k+3], S42,0x8F0CCC92);
           c=II(c,d,a,b,x[k+10],S43,0xFFEFF47D);
           b=II(b,c,d,a,x[k+1], S44,0x85845DD1);
           a=II(a,b,c,d,x[k+8], S41,0x6FA87E4F);
           d=II(d,a,b,c,x[k+15],S42,0xFE2CE6E0);
           c=II(c,d,a,b,x[k+6], S43,0xA3014314);
           b=II(b,c,d,a,x[k+13],S44,0x4E0811A1);
           a=II(a,b,c,d,x[k+4], S41,0xF7537E82);
           d=II(d,a,b,c,x[k+11],S42,0xBD3AF235);
           c=II(c,d,a,b,x[k+2], S43,0x2AD7D2BB);
           b=II(b,c,d,a,x[k+9], S44,0xEB86D391);
           a=AddUnsigned(a,AA);
           b=AddUnsigned(b,BB);
           c=AddUnsigned(c,CC);
           d=AddUnsigned(d,DD);
   		}

   	var temp = WordToHex(a)+WordToHex(b)+WordToHex(c)+WordToHex(d);

   	return temp.toLowerCase();
}



function isEmpty(ob){
   for(var i in ob){ return false;}
  return true;
}
