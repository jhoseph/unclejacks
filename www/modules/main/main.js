/* Start Main Module */

var Main={
    load: function() {
        System.log('loading main..');
        var view= new System.View('main/main');
        view.render('#body',null);
    },
    home: function() {
        this.load();
    },
};

Main.load();

// Implementciaon de todas las funciones (click)
var login = false;

var Implementation = {
    main: function() {
        var view= new System.View('main/main');
        view.render('#body',null);
    },
    poolcard: function() {
        document.getElementById('poolcard_div').innerHTML = '<h2>???</h2>';
    },
    rsvp: function() {
        var view = new System.View('main/html/rsvp');
        view.render('#core',null);
    },
    rsvpdo: function() {
        var view = new System.View('main/html/rsvpdo');
        view.render('#core',null);
    },
    photos: function() {
        var view = new System.View('main/html/photos');
        view.render('#core',null);
    },
    menu: function() {
        var view = new System.View('main/html/menu');
        view.render('#core',null);
    },
    call: function() {
        var view = new System.View('main/html/menu');
        view.render('#core',null);
    },
    events: function() {
        var view = new System.View('main/html/events');
        view.render('#core',null);
    },
    signme: function() {
        var eventdetails = document.getElementById('eventdetails');
        eventdetails.innerHTML = 'You have been added to the tourned list! <br> Don\'t forget the tourney cost \
                                    $10 so bring cash when you come in';
        var eventdetails = document.getElementById('eventmsm');
        eventdetails.innerHTML = '(push me again to take me off the list!)';
    },
    login: function() {
        if(!login) {
            var login = new System.View('main/html/login');
            login.render('#core',null);
            login = true;
        } else {
            var eventview = new System.View('main/html/eventview');
            eventview.render('#core',null);
            login = false;
        }
    },
    vip: function() {
        if(!login) {
            var view = new System.View('main/html/vip');
            view.render('#core',null);
            login = true;
        } else {
            var view = new System.View('main/html/vippage');
            view.render('#core',null);
            login = false;
        }
    }
}

/* End Main Module */

/* Start Helper functions */

function changeImage(imgid,imgpath) {
    var img = document.getElementById(imgid);
    img.src = imgpath;
    fadeImg(img, 100, true);
    setTimeout("changeImage()", 30000);
}

function fadeImg(el, val, fade) {
    if(fade === true){
        val--;
    }else{
        val ++;
    }

    if(val > 50 && val < 100){
        el.style.opacity = val / 100;
        setTimeout(function(){fadeImg(el, val, fade);}, 30);
    }
}

/* End Helper*/